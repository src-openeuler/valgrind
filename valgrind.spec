# LTO triggers undefined symbols in valgrind.  But valgrind has a
# --enable-lto configure time option that we will use instead.
%define _lto_cflags %{nil}

%ifarch %{ix86}
%define arch_val x86
%define arch_old_val %{nil}
%endif
%ifarch x86_64
%define arch_val amd64
%define arch_old_val x86
%endif
%ifarch aarch64
%define arch_val arm64
%define arch_old_val %{nil}
%endif
%ifarch loongarch64
%define arch_val loongarch64
%define arch_old_val %{nil}
%endif
%ifarch ppc64le
%define arch_val ppc64le
%define arch_old_val %{nil}
%endif

Name:           valgrind
Version:        3.22.0
Release:        4
Epoch:          1
Summary:        An instrumentation framework for building dynamic analysis tools
License:        GPL-2.0-or-later
URL:            https://www.valgrind.org/
Source0:        https://sourceware.org/pub/%{name}/%{name}-%{version}.tar.bz2

Patch1:         valgrind-3.9.0-cachegrind-improvements.patch
Patch2:         valgrind-3.9.0-ldso-supp.patch
Patch3:         Add-AArch64-clang-longjmp-support.patch
# Add LOONGARCH64 support
Patch4:         Add-LOONGARCH64-Linux-support.patch
# valgrind-monitor.py regular expressions should use raw strings
# https://bugs.kde.org/show_bug.cgi?id=476708
Patch5:         valgrind-3.22.0-valgrind-monitor-python-re.patch
# valgrind 3.22.0 fails on assertion when loading debuginfo
# https://bugs.kde.org/show_bug.cgi?id=476548
Patch6:         valgrind-3.22.0-rodata.patch
# Add fchmodat2 syscall on linux
# https://bugs.kde.org/show_bug.cgi?id=477198
Patch7:         valgrind-3.22.0-fchmodat2.patch

BuildRequires:  glibc glibc-devel gdb procps gcc-c++ perl(Getopt::Long)
BuildRequires:  automake autoconf
ExclusiveArch:  %{ix86} x86_64 ppc ppc64 ppc64le s390x armv7hl aarch64 loongarch64

%description
Valgrind is an instrumentation framework for building dynamic analysis tools. There are
Valgrind tools that can automatically detect many memory management and threading bugs,
and profile your programs in detail. You can also use Valgrind to build new tools.

%package devel
Summary: Development files for %{name}
Requires: %{name} = %{epoch}:%{version}-%{release}

%description devel
This files contains the development files for %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
# Some patches (might) touch Makefile.am or configure.ac files.
# Just always autoreconf so we don't need patches to prebuild files.
./autogen.sh
CC=%{__cc}
%ifarch x86_64
mkdir -p shared/libgcc/32
ar r shared/libgcc/32/libgcc_s.a
ar r shared/libgcc/libgcc_s_32.a
CC="%{__cc} -B `pwd`/shared/libgcc/"
%endif

%undefine _hardened_build
%undefine _strict_symbol_defs_build
optflags="`echo " %{optflags} -fPIE -fPIC"`"
OPTFLAGS="`echo " $optflags " | sed 's/ -m\(64\|3[21]\) / /g;s/ -fexceptions / /g;s/ -fstack-protector\([-a-z]*\) / / g;s/ -Wp,-D_FORTIFY_SOURCE=2 / /g;s/ -O2 / /g;s/ -mcpu=\([a-z0-9]\+\) / /g;s/^ //;s/ $//'`"

LDFLAGS="`echo " %{build_ldflags} " | sed 's/ -Wl,-z,now / / g;'`"

%configure CC="$CC" CFLAGS="$OPTFLAGS" CXXFLAGS="$OPTFLAGS" LDFLAGS="$LDFLAGS" --with-mpicc=/bin/false GDB=%{_bindir}/gdb --enable-lto
%make_build

%install
%make_install

pushd %{buildroot}%{_libdir}/valgrind/
rm -f *.supp.in *.a
popd

pushd %{buildroot}%{_includedir}/%{name}
rm -rf config.h libvex*h pub_tool_*h vki/
popd

%files
%license COPYING
%doc AUTHORS
%doc %{_datadir}/doc/%{name}/{html,*.pdf}
%exclude %{_datadir}/doc/%{name}/*.ps
%{_bindir}/*
%dir %{_libexecdir}/%{name}
%{_libexecdir}/valgrind/*[^o]
%attr(0755,root,root) %{_libexecdir}/valgrind/vgpreload*-%{arch_val}-*so

%files devel
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc

%files help
%doc NEWS README_*
%{_mandir}/man1/*

%changelog
* Tue Jan 14 2025 Ge Wang <wang__ge@126.com> - 1:3.22.0-4
- Remove empty soft link file

* Sun Oct 06 2024 Funda Wang <fundawang@yeah.net> - 1:3.22.0-3
- Use package's own lto option instead of hardcode LTO flags

* Mon Jan 22 2024 peng.zou <peng.zou@shingroup.cn> - 1:3.22.0-2
- Add ppc64le support

* Mon Jan 08 2024 yaoxin <yao_xin001@hoperun.com> - 1:3.22.0-1
- Upgrade to 3.22.0

* Tue Aug 29 2023 liyunfei <liyunfei33@huawei.com> - 1:3.16.0-7
- Add clang compile support on AArch64

* Tue Jan 03 2023 chenfeiyang <chenfeiyang@loongson.cn> - 1:3.16.0-6
- Sync LoongArch with glibc 2.36

* Fri Dec 16 2022 chenfeiyang <chenfeiyang@loongson.cn> - 1:3.16.0-5
- Add LOONGARCH64/Linux support

* Thu Aug 25 2022 liyanan <liyanan32@h-partners.com> - 1:3.16.0-4
- Add BIND_NOW and PIE safe complie option

* Wed Mar 02 2022 qinyu <qiny16@huawei.com> - 3.16.0-3
- Implement linux rseq syscall as ENOSYS

* Wed Feb 09 2022 gaoxingwang <gaoxingwang@huawei.com> - 3.16.0-2
- backport patch :Generate a ENOSYS (sys_ni_syscall) for clone3 on all linux arches

* Mon Aug 02 2021 shixuantong <shixuantong@huawei.com> - 3.16.0-1
- upgrade version to 3.16.0

* Wed Feb 3 2021 wangjie<wangjie294@huawei.com> - 3.15.0-1
- upgrade 3.15.0

* Sat Dec 7 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.13.0-29
- Package init
